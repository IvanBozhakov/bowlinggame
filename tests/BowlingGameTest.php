<?php
include __DIR__.'/../BowlingGame.php';
class BowlingGameTest extends PHPUnit_Framework_TestCase 
{
	/*
	*@var BowlingGame
	*/
	protected $game;

    protected function setUp()
    {
        $this->game = new BowlingGame();
    }

    /**
     * @test
     */
    public function testThrowTwentyNull()
	{
        for($i=0;$i<20;$i++)
        {
            $this->game->roll(0);
        }
        $this->assertEquals(0,$this->game->score());
    }
    /**
     * @test
     */
    public function testThrowTwentyAndFallOnePin()
	{
        for($i=0;$i<20;$i++)
        {
            $this->game->roll(1);
        }
        $this->assertEquals(20,$this->game->score());
    }

    /**
     * @test
     */
    public function testingSpare()
	{
        $this->game->roll(3);
        $this->game->roll(7);

        $this->game->roll(3);
        $this->assertEquals(16,$this->game->score());
    }

    /**
     * @test
     */
    public function testSrike()
	{
        $this->game->roll(10);//First Frame

        $this->game->roll(3);//Second Frame
        $this->game->roll(4);

        $this->assertEquals(24,$this->game->score());
    }
    /**
     * @test
     */
    public function testPerfectGame(){
        for($i=0;$i<12;$i++){
            $this->game->roll(10);
        }

        $this->assertEquals(300,$this->game->score());
    }

    /**
     * @test
     */
    public function testCustomCase()
	{
        $this->game->roll(10);//Firs frame

        $this->game->roll(6); //Second Frame
        $this->game->roll(4);

        $this->game->roll(2); //Third Frame
        $this->game->roll(5);

        $this->assertEquals(39,$this->game->score());
    }
}
